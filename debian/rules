#! /usr/bin/make -f

ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
export DH_VERBOSE = 1
export PYBUILD_VERBOSE=1
else
export DH_QUIET = 1
endif

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

export PYBUILD_DESTDIR_python3=debian/python3-$(DEB_SOURCE)/

ifneq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
DEB_CXXFLAGS_MAINT_STRIP=-O2
DEB_CXXFLAGS_MAINT_APPEND=-O0
endif

BUILD_DATE=$(shell LC_ALL=C date -u "+%B %d, %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS:=-D today=\"$(BUILD_DATE)\"
SPHINXOPTS+=-D html_last_updated_fmt=\"$(BUILD_DATE)\"

%:
	dh $@ --buildsystem pybuild

override_dh_auto_configure:
	./configure.py --cl-enable-gl

execute_after_dh_auto_build-indep: HOME=$(CURDIR)/debian/.debhelper/generated/_source/home
execute_after_dh_auto_build-indep:
	pybuild --build -i python3 -s custom --build-args 'make -C {dir}/doc html'
	find doc/_build/html/ -name '*.html' -exec sed -r -i \
		-e 's,(<script async="async" )src(="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>),<!-- \1DISABLED\2 -->,' \
		{} +

execute_after_dh_auto_clean:
	$(RM) -r doc/_build/
	$(RM) doc/constants.inc
	$(RM) Makefile siteconf.py
	$(RM) -r __pycache__/
	$(RM) -r tmp/
	$(RM) -r *.egg-info/

override_dh_auto_test:
	dh_auto_test -- -s custom --test-args="PYTHONPATH={build_dir} cp -r {dir}/test {build_dir} && cd {build_dir}/test && {interpreter} -m pytest --verbosity=2 -k 'not test_event_set_callback' && rm -rf {build_dir}/test"

execute_after_dh_installexamples-indep:
	jdupes -r -M debian/python-pyopencl-doc/usr/share/doc/python-pyopencl-doc
	jdupes -r -l debian/python-pyopencl-doc/usr/share/doc/python-pyopencl-doc
